package com.colorata.st.ui.theme

import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

object SDimens{
    val subTitle = 16.sp
    val screenTitle = 60.sp
    val cardTitle = 24.sp
    val buttonText = 14.sp
    val statusCardText = 15.sp

    val roundedCorner = 30.dp
    val borderWidth = 2.dp
    val buttonBorderWidth = 1.dp
    val cardPadding = 10.dp
    val cardHeight = 200.dp
    val postImageSize = 50.dp
    val elementSize = 60.dp

    val smallPadding = 10.dp
    val normalPadding = 20.dp
    val largePadding = 30.dp
}