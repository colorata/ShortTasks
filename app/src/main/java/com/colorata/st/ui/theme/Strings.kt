package com.colorata.st.ui.theme

object Strings{
    const val powerAssistant = "Power Assistant"
    const val next = "Next?"
    const val go = "Go!"
    const val main = "Main"
    const val show = "Show"
    const val relatedPosts = "Related Posts"
    const val power = "Power"
    const val features = "Features"
    const val weatherDirector = "Weather Director"
    const val weatherDirectorSettingsSubTitle = "Weather Director Settings"
    const val powerAssistantSettingsSubTitle = "Power Assistant Settings"
    const val help = "Help"
    const val city = "City"
    const val more = "More"
    const val about = "About"
    const val shortTasksSettingsSubTitle = "ShortTasks Settings"
    const val didntUnderstandSubTitle = "Didn't understand?"
    const val aboutShortTasksSubTitle = "About ShortTasks"
    const val settings = "Settings"
    const val erase = "Erase"
    const val weather = "Weather"
    const val feedback = "Feedback"
    const val donation = "Donation"
    const val version = "Version"
    const val bottomSize = "bottomSize"
    const val shared = "Shared"
    const val screen = "Screen"
    const val nightMode = "nightMode"
    const val theme = "Theme"
    const val mediaVolume = "Media Volume"
    const val ringVolume = "Ring Volume"
    const val brightness = "Brightness"
    const val autoRotate = "Auto Rotation"
    const val dnd = "DND"
    const val nightLight = "Night Light"
    const val flightMode = "Airplane Mode"
    const val baseUrl = "https://api.openweathermap.org/"
    const val appId = "201d8e3dd3a424462228eed61610772d"
    const val minMinDegrees = "-100℃"
    const val minMaxDegrees = "0℃"
    const val maxMinDegrees = "0℃"
    const val maxMaxDegrees = "100℃"
    const val minDegrees = "Minimum Degrees"
    const val maxDegrees = "Maximum Degrees"
    const val save = "Save"
    const val degrees = "Degrees"
    const val add = "Add"
    const val link = "Link"
    const val addApp = "Add App"
    const val addLink = "Add Link"
    const val addAppSubTitle = "Add App Control"
    const val enabled = "Enabled"
    const val disabled = "Disabled"
    const val title = "Title"
    const val systemColor = "System Color"
    const val primaryInt = "primary int"
    const val secondaryInt = "secondary int"
    const val controlColor = "control color"
    const val getStarted = "Get Started"
    const val getStartedSubTitle = "Get Started with STasks"
    const val modifySettings = "Modify Settings"
    const val permissions = "Permissions"
    const val accessibility = "Accessibility"
    const val whyPhone = "STasks uses phone to show you quality of network"
    const val whyAccessibility = "STasks uses accessibility for the best experience"
    const val whyModifySettings = "STasks modifies settings to control device by you"
    const val other = "Other"
    const val alreadyGranted = "Already granted"
    const val ok = "Ok"
    const val linkTitle = "Link Title"
    const val linkLink = "Link Link"
    const val themePicker = "Theme Picker"
    const val themePickerSubTitle = "Create your own ShortTasks"
    const val clearSubTitle = "You'll lose all your data (like City)"
    const val currentTheme = "Current Theme is"
    const val egg1 = "Do you really think you can overplay me?"
    const val egg2 = "Good try, good try..."
    const val egg3 = "Your screen have been destroyed!"
    const val egg4 = "Prompt - where apps are recording your actions?"
    const val currentVersion = "Version: Sunray Compose"
    const val developer = "Developer"
    const val currentDeveloper = "Developer: Colorata"
    const val end = "There is nothing further"
    const val isFirst = "is First"
    const val darkTheme = "Dark Theme"
    const val micro = "Microphone"
    const val whyRoot = "Grant root to STasks for more features!"
    const val hasRoot = "Has Root"
    const val whyDND = "STasks uses DND access to switch DND state"
    const val normalMode = "Normal Mode"
    const val overlay = "Overlay"
    const val whyOverlay = "STasks uses overlay to show you settings"
    const val on = "On"
    const val off = "Off"
    const val trueBackground = "True Background"

    const val powerMainSubTitle = "You will learn how to use Power Assistant"

    const val powerHelpSubTitle = "How to use Power Assistant?"
    const val powerHelp1 = "Go to Accessibility settings and turn on ShortTasks"
    const val powerHelp2 = "Press power button and tap ShortTasks"
    const val powerHelp3 = "Select your elements and tap to the bottom button"

    const val search = "Search"
    const val tethering = "HotSpot"
    const val wifi = "WiFi"
    const val flashlight = "Flashlight"
    const val bluetooth = "Bluetooth"
    const val mobData = "Mobile Data"
    const val nearShare = "Nearby Sharing"
    const val location = "Location"
    const val calc = "Calculator"
    const val batSave = "Battery Saver"
    const val tasks = "Google Tasks"
    const val notify = "Notifications"
    const val button = "Button"
    const val hold = "Hold"
    const val tap = "Tap"
    const val slideOrTap = "Slide or Tap"
    const val battery = "Battery"
    const val time = "Time"
    const val player = "Player"

    const val red = "Maybe Red"
    const val orange = "Peachy Orange"
    const val yellow = "Yellow Marshmallow"
    const val green = "Green Lime"
    const val turquoise = "Turquoise Sea"
    const val blue = "Natural Blue"
    const val purple = "Very Purple"
    const val white = "Boring White"
    const val autoDetect = "Personal Designer"

    const val dotIcon = "•"
    const val degreeIcon = "℃"
    const val rawDegreeIcon = "\u00B0"
    const val percentFormat = "%1.0f%%"

    const val googleClockApp = "com.google.android.deskclock"
}